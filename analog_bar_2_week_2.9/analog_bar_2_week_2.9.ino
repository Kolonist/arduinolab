#define LED1 2
#define LED10 11
#define LED_ON 2

#define ANALOG_IN A0

void setup() {
  for(int i = LED1; i <= LED10; i++)
    pinMode(i, OUTPUT);
    
}

void loop() {
 int lastLed = map(analogRead(ANALOG_IN), 0, 1023, 2, 11);
  
 for(int i = LED1; i <= lastLed; i+=1)

 {
    digitalWrite(i, HIGH);
    delay(LED_ON);
    digitalWrite(i, LOW);
  } 

}
