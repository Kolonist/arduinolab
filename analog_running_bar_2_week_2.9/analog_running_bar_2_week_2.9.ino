#define LED1 2
#define LED10 11
#define LED_ON 2

#define ANALOG_IN A0

#define BUTTON_PIN 12

boolean trigger = false;
//boolean previous = false;

int i;

void setup() {
  for(int i = LED1; i <= LED10; i++)
    pinMode(i, OUTPUT);

    pinMode(BUTTON_PIN, INPUT);
    
}

void loop() {
  if(digitalRead(BUTTON_PIN))
  {
    i = 5;
  } 
  else
  {
    i = 2;
  }
  
  digitalWrite(i, HIGH);
  delay(LED_ON);
  digitalWrite(i, LOW);

}
