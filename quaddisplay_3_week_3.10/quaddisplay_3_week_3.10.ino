#include <QuadDisplay.h>

#define ECHO_PIN 8
#define TRIG_PIN 9

#define DISPLAY_PIN 2

void setup() {
  Serial.begin(9600);
  pinMode(ECHO_PIN, INPUT);
  pinMode(TRIG_PIN, OUTPUT);

  pinMode(DISPLAY_PIN, OUTPUT);
}

void loop() {
  //Serial.println(measure());
  displayInt(DISPLAY_PIN, measure());

}

int measure()
{
  digitalWrite(TRIG_PIN, HIGH);
  digitalWrite(TRIG_PIN, LOW);
  int distance = pulseIn(ECHO_PIN, HIGH, 15000) / 50;

  return constrain(distance, 1, 300);
}

