#define LED1 2
#define LED10 11
#define LED_ON 200

void setup() {
  for(int i = LED1; i <= LED10; i++)
    pinMode(i, OUTPUT);
    
}

void loop() {
 for(int i = LED10; i >= LED1; i-=1)
 // int i = 11; test
 {
    digitalWrite(i, HIGH);
    delay(LED_ON);
    digitalWrite(i, LOW);
  } 

}
