#define LED_PIN 3
#define ECHO_PIN 8
#define TRIG_PIN 9

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);
  analogWrite(LED_PIN, HIGH);

}

void loop() {
  digitalWrite(TRIG_PIN, HIGH);
  digitalWrite(TRIG_PIN, LOW);
  int distance = pulseIn(ECHO_PIN, HIGH, 15000) / 50;
  //Serial.print("Distanse:");
  Serial.println(distance);
  analogWrite(LED_PIN, HIGH);
  //Serial.println("High");
  delay(1000);
  analogWrite(LED_PIN, LOW);
  //Serial.println("Low");
  delay(1000);
  analogWrite(LED_PIN, map(distance,0,1023,0,255));
}
